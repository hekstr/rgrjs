import fs from 'fs'
import express from 'express'
import Schema from './data/schema'
import GraphQLHTTP from 'express-graphql'
import {MongoClient} from 'mongodb'
import {graphql} from 'graphql'
import {introspectionQuery} from 'graphql/utilities'

let app = express()
app.use(express.static('public'))

;(async function() {
  try{
    let uri = 'mongodb://mo1338_rgrjs:LORD00system@mongo4.mydevil.net:27017/mo1338_rgrjs'
    let db = await MongoClient.connect(uri)
    let schema = Schema(db)
    app.use('/graphql', GraphQLHTTP({
      schema,
      graphiql: true
    }))

    app.listen(3000, () => console.log('Listening on port 3000'))

    // Generate schema.json for Relay
    let json = await graphql(schema, introspectionQuery)
    fs.writeFile('./data/schema.json', JSON.stringify(json, null, 2), err => {
      if(err) {
        throw err
      }

      console.log('Json schema created')
    })
  } catch(e) {
    console.log(e)
  }
}())
