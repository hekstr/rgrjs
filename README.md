## RGR.js

React.js, Express.js server using Webpack with Babel, Relay, GraphQL

## Installing

npm install

npm install -g webpack nodemon

- Run a webpack watcher in one terminal: `webpack -w -d`
- Run a nodemon process in another terminal: `npm start`
- Go to http://localhost:3000
